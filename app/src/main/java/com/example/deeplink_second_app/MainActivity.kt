package com.example.deeplink_second_app

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.Button
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.tooling.preview.Preview
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument
import androidx.navigation.navDeepLink
import com.example.deeplink_second_app.ui.theme.DeeplinksecondappTheme

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            DeeplinksecondappTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colors.background
                ) {
                    TestLayout()
                }
            }
        }
    }
}

@Composable
fun TestButton(name: String) {
    val context = LocalContext.current
    // getting the data from our

    Button(onClick = {
        val urlIntent = Intent(
            Intent.ACTION_VIEW,
            Uri.parse("https://www." + "felipevogt.com/firstApp")
        )
        context.startActivity(urlIntent)
    }) {
        Text(text = name)
    }
}

@Composable
fun TestLayout() {
    val navController = rememberNavController()
    NavHost(navController = navController, startDestination = "home") {
        composable(route = "home") {
            Column(
                modifier = Modifier.fillMaxSize(),
                verticalArrangement = Arrangement.Center,
                horizontalAlignment = Alignment.CenterHorizontally
            ) {
                TestButton("Go to first app")
            }
        }
        composable(
            route = "secondApp",
            deepLinks = listOf(
                navDeepLink {
                    uriPattern = "https://www.felipevogt.com/secondApp?text={text}&number={number}"
                }
            ),
            arguments = listOf(
                navArgument("text") {
                    type = NavType.StringType
                    defaultValue = ""
                },
                navArgument("number") {
                    type = NavType.IntType
                    defaultValue = 0
                }
            )
        ) { entry ->
            val text = entry.arguments?.getString("text")
            val number = entry.arguments?.getInt("number")

            Column(
                modifier = Modifier.fillMaxSize(),
                verticalArrangement = Arrangement.Center,
                horizontalAlignment = Alignment.CenterHorizontally
            ) {
                Text(text = "text value: $text and number value: $number")
                TestButton("Go to first app")
            }
        }
    }


}

@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
    DeeplinksecondappTheme {
        Surface(
            modifier = Modifier.fillMaxSize(),
            color = MaterialTheme.colors.background
        ) {
            TestLayout()
        }
    }
}